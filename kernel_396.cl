// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
} Tuple2_float_float;
#endif

float multAndSumUp(float acc, float l, float r){
    {
        { return acc + (l * r); }; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__65, const global float* restrict v__66, const global float* restrict v__67, global float* v__79, global float* v__73, int v_K_3, int v_M_1, int v_N_0, int v_O_2){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        float v__68; 
        float v__74; 
        // Private Memory
int v_gl_id_58 = get_global_id(1);
        //for (int v_gl_id_58 = get_global_id(1); (v_gl_id_58 < v_M_1); v_gl_id_58 = (1 + v_gl_id_58))
		   {
            // map_seq
            for (int v_i_59 = 0; (v_i_59 < v_N_0); v_i_59 = (1 + v_i_59)){
                float v_tmp_95 = 0.0f; 
                v__68 = v_tmp_95; 
                // reduce_seq
                for (int v_i_60 = 0; (v_i_60 < v_K_3); v_i_60 = (1 + v_i_60)){
                    v__68 = multAndSumUp(v__68, v__65[(v_i_60 + (v_K_3 * v_gl_id_58))], v__66[(v_i_59 + (v_N_0 * v_i_60))]); 
                }
                // end reduce_seq
                // map_seq
                // iteration count is exactly 1, no loop emitted
                {
                    int v_i_61 = 0; 
                    v__73[(v_i_59 + (v_N_0 * v_gl_id_58))] = id(v__68); 
                }
                // end map_seq
            }
            // end map_seq
int v_gl_id_62 = get_global_id(0);
            //for (int v_gl_id_62 = get_global_id(0); (v_gl_id_62 < v_O_2); v_gl_id_62 = (1 + v_gl_id_62))
		       {
                float v_tmp_98 = 0.0f; 
                v__74 = v_tmp_98; 
                // reduce_seq
                for (int v_i_63 = 0; (v_i_63 < v_N_0); v_i_63 = (1 + v_i_63)){
                    v__74 = multAndSumUp(v__74, v__67[(v_gl_id_62 + (v_O_2 * v_i_63))], v__73[(v_i_63 + (v_N_0 * v_gl_id_58))]); 
                }
                // end reduce_seq
                // map_seq
                // iteration count is exactly 1, no loop emitted
                {
                    int v_i_64 = 0; 
                    v__79[(v_gl_id_62 + (v_O_2 * v_gl_id_58))] = id(v__74); 
                }
                // end map_seq
            }
        }
    }
}
